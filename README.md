# Composer

This project hosts a git repository and a Docker image registry for running `docker-compose` on GitLab CI (Continuous Integration) pipelines.

Example use:

```
stages:
  - build

build:
  stage: build
  image: registry.gitlab.com/henrikstroem/composer:latest
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  before_script:
    - docker login -u gitlab-ci-token -p "$GITLAB_CI_TOKEN" "$CI_REGISTRY"
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE/<IMAGE_NAME>:latest" .
    - RTE=test docker-compose up --abort-on-container-exit --exit-code-from app
    - docker push "$CI_REGISTRY_IMAGE/<IMAGE_NAME>:latest"
```

Simply replace `<IMAGE_NAME>` with an appropriate name.
